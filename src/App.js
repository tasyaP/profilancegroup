import React from 'react';
import {Route} from "react-router-dom";

import './App.css';
import './css/style.css';

import StartBlockContainer from "./components/startBlock/StartBlockContainer";
import HeaderContainer from "./components/header/HeaderContainer";
import NewsContainer from "./components/news/NewsContainer";

const App = () => {
    return (
        <div className='app-wrapper'>
            <HeaderContainer/>
            <div className="content">
                <Route exact path='/' render={() => <StartBlockContainer/>}/>
                <Route path='/news' render={() => <NewsContainer/>}/>
            </div>
        </div>
    );
};

export default App;
