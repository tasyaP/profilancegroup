import React, {useState} from 'react';

import s from './news.module.css';

import NewsItem from "./newsItem/NewsItem";
import AddNews from "./addNews/AddNews";

const News = (props) => {
    let [text, setText] = useState('');
    return <>
        <div className={s.news}>
            <input type="text"
                   placeholder='Поиск'
                   value={text}
                   onChange={event => {
                       setText(event.target.value);
                       props.filterNews(event.target.value);
                   }}/>
            <div className={s.news__list}>
                {
                    props.listNews.map((item) => {
                        return <NewsItem news={item}
                                         key={item.id}
                                         id={item.id}
                                         isAuth={props.isAuth}
                                         isAdmin={props.isAdmin}
                                         approve={props.approvedNews}
                                         deleteNews={props.deleteNews}/>
                    })
                }
            </div>
            {
                props.isAuth
                    ? <button onClick={() => props.openPopup()}>Добавить Новость</button>
                    : null

            }

        </div>
        {
            props.isOpenPopup
                ? <AddNews closePopup={props.closePopup}
                           addNews={props.addNews}/>
                : ''
        }
    </>
};

export default News;