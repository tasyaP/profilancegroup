import {connect} from "react-redux";
import News from "./News";
import {
    addNews,
    approvedNews,
    closeAddNewsPopup,
    deleteNews,
    filterNews,
    openAddNewsPopup
} from "../../redux/newsReducer";


let mapStateToProps = (state) => {
    return {
        listNews: state.news.listNews,
        isOpenPopup: state.news.isPopupAddNewsVisible,
        isAuth: state.content.isAuth,
        isAdmin: state.content.user.isAdmin
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        openPopup: () => {
            dispatch(openAddNewsPopup());
        },
        closePopup: () => {
            dispatch(closeAddNewsPopup());
        },
        approvedNews: (idNews) => {
            dispatch(approvedNews(idNews));
        },
        deleteNews: (idNews) => {
            dispatch(deleteNews(idNews));
        },
        addNews: (title, description) => {
            dispatch(addNews(title, description));
        },
        filterNews: (text) => {
            dispatch(filterNews(text));
        }
    }
};

const NewsContainer = connect(mapStateToProps, mapDispatchToProps)(News);

export default NewsContainer;