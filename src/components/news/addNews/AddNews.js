import React, {useState} from 'react';

import s from './../../authorization/auth.module.css';

const AddNews = (props) => {
    let [title, setTitle] = useState('');
    let [description, setDescription] = useState('');

    return <div className='overlay'>
        <form className={s.popup}>
            <div className="close" onClick={() => props.closePopup()}>x</div>
            <label>
                <span> Название новости</span>
                <input type="text"
                       value={title}
                       onChange={event => setTitle(event.target.value)}/>
            </label>
            <label>
                <span> Описание* </span>
                <textarea value={description} onChange={event => setDescription(event.target.value)}></textarea>
            </label>
            <button onClick={event => {
                event.preventDefault();
                if (title != '' && description != '') {
                    props.addNews(title, description)
                }
            }}>Добавить
            </button>
        </form>
    </div>
};

export default AddNews;
