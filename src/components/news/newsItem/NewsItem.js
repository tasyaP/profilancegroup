import React from 'react';
import s from "../news.module.css";

const NewsItem = (props) => {
    return <>
        {
            (props.news.approved || props.isAuth) && props.news.isVisible
                ? <div className={s.news__item}>
                    {
                        props.isAuth && props.isAdmin
                            ? <div className={s.news__top}>
                                <button className='delete' onClick={() => props.deleteNews(props.id)}>Удалить</button>
                                {props.news.approved ? '' : <button onClick={() => props.approve(props.id)}>Одобрить</button>}
                            </div>
                            : null
                    }

                    <a href="#" className={s.news__title}>{props.news.title}</a>
                    <div className={s.news__description}>
                        {props.news.description}
                    </div>
                    <div className={s.news__bottom}>
                        <div className={s.news__date}>{props.news.dateCreate}</div>
                        {
                            props.news.approved
                                ? <div className='status approved'> Одобрено</div>
                                : <div className='status not-approved'>На одобрении</div>
                        }
                    </div>
                </div>
                : null
        }

    </>
};

export default NewsItem;