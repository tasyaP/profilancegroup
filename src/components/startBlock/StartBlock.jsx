import React from 'react';

const StartBlock = (props) => {
    return <div>Привет,
        {
            props.isAuth
                ? props.user.name
                : props.message
        }
    </div>
};


export default StartBlock;