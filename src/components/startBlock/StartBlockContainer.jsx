import {connect} from "react-redux";
import StartBlock from "./StartBlock";


let mapStateToProps = (state) => {
    return {
        message: state.content.defaultMessage,
        isAuth: state.content.isAuth,
        user: state.content.user
    }
};

const startBlockContainer = connect(mapStateToProps,)(StartBlock);

export default startBlockContainer;