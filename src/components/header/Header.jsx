import React from 'react';
import s from './header.module.css';
import {NavLink} from "react-router-dom";
import AuthContainer from "../authorization/AuthContainer";

const Header = (props) => {
    return <>
        <header>
            <div className={s.header__menu}>
                <div className={s.header__link}>
                    <NavLink to="/">Главная</NavLink>
                </div>
                <div className={s.header__link}>
                    <NavLink to="/news">Новости</NavLink>
                </div>
            </div>
            <div className={s.header__link}>
                {
                    props.isAuth
                        ? <a href='#' onClick={() => props.logout()}>Выход</a>
                        : <a href='#' onClick={() => props.openPopupAuth()}>Вход</a>
                }

            </div>
        </header>

        {props.isOpenAuth ? <AuthContainer/> : ''}
    </>
};


export default Header;