import {connect} from "react-redux";
import Header from "./Header";
import {closeAuthPopup, logout, openAuthPopup} from "../../redux/contentReducer";


let mapStateToProps = (state) => {/*не забывать указывать ветку редюсера*/
    return {
        isOpenAuth: state.content.isAuthPopupVisible,
        isAuth: state.content.isAuth
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        openPopupAuth: () => {
            dispatch(openAuthPopup());
        },
        closePopupAuth: () => {
            dispatch(closeAuthPopup());
        },
        logout: () => {
            dispatch(logout());
        }
    }
};

const HeaderContainer = connect(mapStateToProps, mapDispatchToProps)(Header);

export default HeaderContainer;