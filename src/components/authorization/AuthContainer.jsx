import {connect} from "react-redux";
import {auth, closeAuthPopup} from "../../redux/contentReducer";
import Auth from "./Auth";


let mapStateToProps = (state) => {
    return {
        isAuth: state.content.isAuth,
        isError: state.content.isError
    }
};

let mapDispatchToProps = (dispatch) => {
    return {
        auth: (login, password) => {
            dispatch(auth(login, password));
        },
        closePopup: () => {
            dispatch(closeAuthPopup());
        }
    }
};

const startBlockContainer = connect(mapStateToProps, mapDispatchToProps)(Auth);

export default startBlockContainer;