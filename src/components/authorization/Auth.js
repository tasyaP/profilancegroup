import React, {useState} from 'react';

import s from './auth.module.css';

const Auth = (props) => {
    let [login, setLogin] = useState('');
    let [password, setPassword] = useState('');

    return <div className='overlay'>
        <form className={s.popup} autoComplete="off">
            <div className="close" onClick={() => props.closePopup()}>x</div>
            <label>
                <span> Логин*</span>
                <input type="text"
                       autoComplete="off"
                       value={login}
                       onChange={event => setLogin(event.target.value)}/>
            </label>
            <label>
                <span> Пароль* </span>
                <input type="password"
                       autoComplete="off"
                       value={password}
                       onChange={event => setPassword(event.target.value)}/>
            </label>
            {
                props.isError
                    ? <span className='error'>Не верно введен логин или пароль</span>
                    : null
            }
            <div className="submit" onClick={event => {
                event.preventDefault()
                props.auth(login, password)
            }}>Войти
            </div>
        </form>
    </div>
};

export default Auth;
