import {combineReducers, createStore} from "redux";
import contentReducer from "./contentReducer";
import newsReducer from "./newsReducer";


let reducers = combineReducers({
    content: contentReducer,
    news: newsReducer
});

let store = createStore(reducers);

window.store = store;

export default store;