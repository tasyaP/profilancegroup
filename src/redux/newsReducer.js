/*const*/
const IS_OPEN_POPUP_ADD_NEWS = 'IS_OPEN_POPUP_ADD_NEWS';
const IS_CLOSE_POPUP_ADD_NEWS = 'IS_CLOSE_POPUP_ADD_NEWS';
const APPROVED = 'APPROVED';
const DELETE_NEWS = 'DELETE_NEWS';
const ADD_NEWS = 'ADD_NEWS';
const FILTER_NEWS = 'FILTER_NEWS';

/*initial state*/
let initialState = {
    listNews: [
        {
            id: 0,
            title: 'Горе-грибников спасли сотрудники ГИБДД',
            description: 'В сентябре четыре женщины с двумя детьми поехали за грибами, но заблудились в лесу недалеко от поселка Котомыш Пермского края. Об этом сообщили в дежурную часть отдела МВД России по Красновишерскому району, передает пресс-служба МВД России. Информация немедленно была передана экипажу ДПС ГИБДД.',
            approved: false,
            dateCreate: '24.09.2020',
            isVisible: true
        },
        {
            id: 1,
            title: 'В Красноярском крае из зерна будут производить биопластик',
            description: 'В сентябре четыре женщины с двумя детьми поехали за грибами, но заблудились в лесу недалеко от поселка Котомыш Пермского края. Об этом сообщили в дежурную часть отдела МВД России по Красновишерскому району, передает пресс-служба МВД России. Информация немедленно была передана экипажу ДПС ГИБДД.',
            approved: true,
            dateCreate: '24.09.2020',
            isVisible: true
        },
        {
            id: 2,
            title: 'Вернуть долг природе: в Якутии открылся первый лесной питомник',
            description: 'В сентябре четыре женщины с двумя детьми поехали за грибами, но заблудились в лесу недалеко от поселка Котомыш Пермского края. Об этом сообщили в дежурную часть отдела МВД России по Красновишерскому району, передает пресс-служба МВД России. Информация немедленно была передана экипажу ДПС ГИБДД.',
            approved: true,
            dateCreate: '24.09.2020',
            isVisible: true
        },
        {
            id: 3,
            title: 'В Крыму впервые посчитали каждое дерево',
            description: 'В сентябре четыре женщины с двумя детьми поехали за грибами, но заблудились в лесу недалеко от поселка Котомыш Пермского края. Об этом сообщили в дежурную часть отдела МВД России по Красновишерскому району, передает пресс-служба МВД России. Информация немедленно была передана экипажу ДПС ГИБДД.',
            approved: false,
            dateCreate: '24.09.2020',
            isVisible: true
        }
    ],
    isPopupAddNewsVisible: false
};

/*reducer*/
const contentReducer = (state = initialState, action) => {
    switch (action.type) {
        case IS_OPEN_POPUP_ADD_NEWS: {
            return {
                ...state,
                isPopupAddNewsVisible: true
            }
        }
        case IS_CLOSE_POPUP_ADD_NEWS: {
            return {
                ...state,
                isPopupAddNewsVisible: false
            }
        }
        case APPROVED: {
            return {
                ...state,
                listNews: state.listNews.map(function (news, index) {
                    if (action.idNews === news.id) {
                        return {...news, approved: true}
                    }
                    return news;
                })
            }
        }
        case DELETE_NEWS: {
            let newListNews = [];
            listNews: state.listNews.map(function (news, index) {
                if (action.idNews !== news.id) {
                    newListNews.push(news);
                }
            });
            return {
                ...state,
                listNews: newListNews
            }
        }
        case ADD_NEWS: {
            function formatDate(date) {

                var dd = date.getDate();
                if (dd < 10) dd = '0' + dd;

                var mm = date.getMonth() + 1;
                if (mm < 10) mm = '0' + mm;

                var yy = date.getFullYear();

                return dd + '.' + mm + '.' + yy;
            }

            let newNews = {
                id: Math.random(),
                title: action.title,
                description: action.description,
                approved: false,
                dateCreate: formatDate(new Date()),
                isVisible: true
            };
            return {
                ...state,
                listNews: [...state.listNews, newNews],
                isPopupAddNewsVisible: false
            }
        }
        case FILTER_NEWS: {
            let text = action.text.toLowerCase();
            return {
                ...state,
                listNews: state.listNews.map(function (news, index) {
                    if (news.title.toLowerCase().indexOf(text) >= 0) {
                        return {...news, isVisible: true}
                    } else {
                        return {...news, isVisible: false}
                    }
                })
            }
        }
        default:
            return state;
    }
};

/*action create*/
export const openAddNewsPopup = () => ({type: IS_OPEN_POPUP_ADD_NEWS});
export const closeAddNewsPopup = () => ({type: IS_CLOSE_POPUP_ADD_NEWS});
export const approvedNews = (idNews) => ({type: APPROVED, idNews});
export const deleteNews = (idNews) => ({type: DELETE_NEWS, idNews});
export const addNews = (title, description) => ({type: ADD_NEWS, title, description});
export const filterNews = (text) => ({type: FILTER_NEWS, text});

export default contentReducer;