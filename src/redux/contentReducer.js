/*const*/
const IS_OPEN_POPUP_AUTH = 'IS_OPEN_POPUP_AUTH';
const IS_CLOSE_POPUP_AUTH = 'IS_CLOSE_POPUP_AUTH';
const AUTH = 'AUTH';
const LOG_OUT = 'LOG_OUT';

/*initial state*/
let initialState = {
    isAuth: false,
    user: {
        name: '',
        isAdmin: false
    },
    defaultMessage: 'Гость',
    users: [
        {
            login: 'admin',
            password: 'admin',
            isAdmin: true
        },
        {
            login: 'user',
            password: 'user',
            isAdmin: false
        }
    ],
    isAuthPopupVisible: false,
    isError: false
};

/*reducer*/
const contentReducer = (state = initialState, action) => {
    switch (action.type) {
        case IS_OPEN_POPUP_AUTH: {
            return {
                ...state,
                isAuthPopupVisible: true
            }
        }
        case IS_CLOSE_POPUP_AUTH: {
            return {
                ...state,
                isAuthPopupVisible: false
            }
        }
        case AUTH: {
            let text, isAdmin;
            state.users.map(function (user, index) {
                if (user.login === action.login && user.password === action.password) {
                    text = action.login;
                    isAdmin = user.isAdmin
                }
            });
            return {
                ...state,
                isAuth: text ? true : false,
                user: {name: text, isAdmin: isAdmin},
                isAuthPopupVisible: text ? false : true,
                isError: text ? false : true
            }
        }
        case LOG_OUT: {
            return {
                ...state,
                isAuth: false
            }
        }
        default:
            return state;
    }
};

/*action create*/
export const openAuthPopup = () => ({type: IS_OPEN_POPUP_AUTH});
export const closeAuthPopup = () => ({type: IS_CLOSE_POPUP_AUTH});

export const auth = (login, password) => ({
    type: AUTH,
    login: login,
    password: password
});
export const logout = () => ({
    type: LOG_OUT
});

export default contentReducer;